
**⚠ WARNING: This project has been moved to [gitlab.incoresemi.com](https://gitlab.incoresemi.com).
It will soon be archived and eventually deleted.**

**Simulating on Spike**

--------------

Spike, the RISC-V ISA Simulator, implements a functional model of one or more RISC-V harts. It is named after the golden spike used to celebrate the completion of the US transcontinental railway.

Spike supports the following RISC-V ISA features:

RV32I and RV64I base ISAs, v2.1
Zifencei extension, v2.0
Zicsr extension, v2.0
M extension, v2.0
A extension, v2.0
F extension, v2.2
D extension, v2.2
Q extension, v2.2
C extension, v2.0
V extension, v0.7.1
Conformance to both RVWMO and RVTSO (Spike is sequentially consistent)
Machine, Supervisor, and User modes, v1.7
Debug v0.14

In later versions above 1.7+ the Htif support has been excluded,hence one will not be able to boot linux with a filesystem.

**Build Instructions**
---------------

Download and install the riscv-tools here [riscv-tools](https://gitlab.com/shaktiproject/software/riscv-tools/tree/priv-spec-1.7)

**Building linux**
-------------------
Clone the above repository and follow the steps below to build linux

For building the initramfs ,go to arch/riscv/configs,in the defconfig file add the following 
  
     CONFIG_INITRAMFS_SOURCE="initramfs.cpio.gz"

Next step is to build linux by doing the following.

     $ make ARCH=riscv CROSS_COMPILE=<path-to-toolcahin>/riscv64-unknown-linux-gnu- defconfig
     $ make ARCH=riscv CROSS_COMPILE=<path-to-toolcahin>/riscv64-unknown-linux-gnu- -j4

For 32bit linux kernel use the "riscv32-unknown-linux-gnu-" instead.

Next step is to build the generated binary(vmlinux) atop the proxy kernel      

**RISC-V PK**
----------------
**About**
---------------
The RISC-V Proxy Kernel, pk, is a lightweight application execution environment that can host statically-linked RISC-V ELF binaries. It is designed to support tethered RISC-V implementations with limited I/O capability and and thus handles I/O-related system calls by proxying them to a host computer.

This package also contains the Berkeley Boot Loader, bbl, which is a supervisor execution environment for tethered RISC-V systems. 

**Building the PK**
---------------------

Clone the PK here [RISCV-PK](https://github.com/riscv/riscv-pk.git)

We assume that the RISCV environment variable is set to the RISC-V tools install path, and that the riscv-gnu-toolchain package is installed. Please note that building the binaries directly inside the source directory is not supported; you need to use a separate build directory.

       $ mkdir build
       $ cd build
       $ ../configure --prefix=$RISCV --host=riscv64-unknown-elf --with-payload=<path-to-your-vmlinux> --enable-logo
       $ make bbl

**Simulating on Spike**
------------------------
     $ spike  bbl
